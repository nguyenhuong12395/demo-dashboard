import { Component } from '@angular/core';
import { ChartConfiguration, ChartOptions } from 'chart.js';
// import {valueOrDefault} from '../../node_modules/chart.js/dist/helpers.mjs';
import colorLib from '@kurkle/color';

const CHART_COLORS = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};
export function valueOrDefault(value, defaultValue?) {
  return typeof value === 'undefined' ? defaultValue : value;
}

export function rand(min?, max?) {
 let _seed = Date.now()
  min = valueOrDefault(min, 0);
  max = valueOrDefault(max, 0);
  _seed = (_seed * 9301 + 49297) % 233280;
  return min + (_seed / 233280) * (max - min);
}


export function transparentize(value, opacity?) {
  var alpha = opacity === undefined ? 0.5 : 1 - opacity;
  return colorLib(value).alpha(alpha).rgbString();
}

export function numbers(config) {
  var cfg = config || {};
  var min = valueOrDefault(cfg.min, 0);
  var max = valueOrDefault(cfg.max, 100);
  var from = valueOrDefault(cfg.from, []);
  var count = valueOrDefault(cfg.count, 8);
  var decimals = valueOrDefault(cfg.decimals, 8);
  var continuity = valueOrDefault(cfg.continuity, 1);
  var dfactor = Math.pow(10, decimals) || 0;
  var data = [];
  var i, value;

  for (i = 0; i < count; ++i) {
    value = (from[i] || 0) + rand(min, max);
    if (rand() <= continuity) {
      data.push(Math.round(dfactor * value) / dfactor);
    } else {
      data.push(null);
    }
  }

  return data;
}
const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

export function months(config) {
  var cfg = config || {};
  var count = cfg.count || 12;
  var section = cfg.section;
  var values = [];
  var i, value;

  for (i = 0; i < count; ++i) {
    value = MONTHS[Math.ceil(i) % 12];
    values.push(value.substring(0, section));
  }

  return values;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'ng2-charts-demo';
  public inputs = {
    min: 20,
    max: 80,
    count: 8,
    decimals: 2,
    continuity: 1
  };

  public doughnutChartLabels: string[] = [ 'Download Sales', 'In-Store Sales', 'Mail-Order Sales' ];
  public doughnutChartDatasets: ChartConfiguration<'doughnut'>['data']['datasets'] =  [{
    label: 'My First Dataset',
    data: [300, 50, 100],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)'
    ],
    hoverOffset: 4
  }];

  public doughnutChartOptions: ChartConfiguration<'doughnut'>['options'] = {
    responsive: false
  };

  public lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July'
    ],
    datasets: [
      {
        label: 'D0',
        data: this.generateData(),
        borderColor: CHART_COLORS.red,
        backgroundColor: transparentize(CHART_COLORS.red),
        hidden: true
      },
      {
        label: 'D1',
        data: this.generateData(),
        borderColor: CHART_COLORS.orange,
        backgroundColor: transparentize(CHART_COLORS.orange),
        fill: '-1'
      },
      {
        label: 'D2',
        data: this.generateData(),
        borderColor: CHART_COLORS.yellow,
        backgroundColor: transparentize(CHART_COLORS.yellow),
        hidden: true,
        fill: 1
      },
      {
        label: 'D3',
        data: [0, 60.64, 60.64, 60.64, 30, 20, 60.64, -5],
        borderColor: CHART_COLORS.green,
        backgroundColor: transparentize(CHART_COLORS.green),
        fill: '-1'
      },
      {
        label: 'D4',
        data: [-10, 20, 30, 0, -10, 20, 60.64, -20,-7.5],
        borderColor: CHART_COLORS.blue,
        backgroundColor: transparentize(CHART_COLORS.blue),
        fill: '-1'
      },
      {
        label: 'D5',
        data: [-8.5, 20, 40, 5, -15, 18, 50, -20,-7.5],
        borderColor: CHART_COLORS.grey,
        backgroundColor: transparentize(CHART_COLORS.grey),
        fill: '+2'
      },
      {
        label: 'D6',
        data: this.generateData(),
        borderColor: CHART_COLORS.purple,
        backgroundColor: transparentize(CHART_COLORS.purple),
        fill: false
      },
      {
        label: 'D7',
        data: this.generateData(),
        borderColor: CHART_COLORS.red,
        backgroundColor: transparentize(CHART_COLORS.red),
        fill: 8
      },
      {
        label: 'D8',
        data: this.generateData(),
        borderColor: CHART_COLORS.orange,
        backgroundColor: transparentize(CHART_COLORS.orange),
        fill: 'end',
        hidden: true
      },
      {
        label: 'D9',
        data: this.generateData(),
        borderColor: CHART_COLORS.yellow,
        backgroundColor: transparentize(CHART_COLORS.yellow),
        fill: { above: 'blue', below: 'red', target: { value: 350 } }
      }
    ]
  };
  public lineChartOptions: ChartOptions<'line'> = {
    responsive: false
  };
  public lineChartLegend = true;

  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartConfiguration<'bar'>['data'] = {
    labels: [ '2006', '2007', '2008', '2009', '2010', '2011', '2012' ],
    datasets: [
      { data: [ 65, 59, 80, 81, 56, 55, 40 ], label: 'Series A' },
      { data: [ 28, 48, 40, 19, 86, 27, 90 ], label: 'Series B' }
    ]
  };

  public barChartOptions: ChartConfiguration<'bar'>['options'] = {
    responsive: false,
  };

  public pieChartOptions: ChartOptions<'pie'> = {
    responsive: false,
  };
  public pieChartLabels = [ [ 'Download', 'Sales' ], [ 'In', 'Store', 'Sales' ], 'Mail Sales' ];
  public pieChartDatasets = [ {
    data: [ 300, 500, 100 ]
  } ];
  public pieChartLegend = true;
  public pieChartPlugins = [];

  barChartData1 = {
    labels: [
      'January',
      'February',
      'March',
      'April'
    ],
    datasets: [{
      type: 'bar',
      label: 'Bar Dataset',
      data: [10, 20, 30, 40],
      borderColor: 'rgb(255, 99, 132)',
      backgroundColor: 'rgba(255, 99, 132, 0.2)'
    }, {
      type: 'line',
      label: 'Line Dataset',
      data: [0, 20, 10, 15],
      fill: false,
      borderColor: 'rgb(54, 162, 235)'
    }]
  };
  barChartOptions1 = {
    type: 'scatter',
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  };

  constructor() {
  }

  generateLabels() {
    return months({ count: this.inputs.count });
  };

  generateData() { return numbers(this.inputs) };

  ngOnInit() {
    console.log(this.lineChartData.datasets);
    
  }

}

